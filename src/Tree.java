import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/*
    * A simple binary tree
    * This class contains a simple way to print out the examples of Depth-First Search traversals
 */
public class Tree {

    int data;
    Tree left;
    Tree right;

    public Tree(int data) {
        this.data = data;
        this.left = null;
        this.right = null;
    }

    public void add(int data) {

        // go right.
        if(data >= this.data) {
            if(this.right == null) {
                this.right = new Tree(data); // create a new node i.e. tree in the empty spot.
            } else {
                this.right.add(data); // recurse and look for an empty spot.
            }
        }

        // go left
        if(data < this.data ) {
            if(this.left == null) {
                this.left = new Tree(data);
            } else  {
                this.left.add(data);
            }
        }

    }


    /*
        * Preorder traverse the tree.
        * Visit node, then left, then right.
        * Recursive
     */
    public void printPreorder(Tree tree) {


        System.out.print(tree.data + " "); // print node.

        if(tree.left != null) {
            printPreorder(tree.left);
        }

        if(tree.right != null) {
            printPreorder(tree.right);
        }
    }

    /*
        * Inorder traverse the tree
        * Visit left, then node, then right.
        * Recursive
     */
    public void printInOrder(Tree tree) {

        if(tree.left != null) {
            printInOrder(tree.left);
        }

        System.out.print(tree.data + " "); // print node.


        if(tree.right != null) {
            printInOrder(tree.right);
        }
    }


    /*
        * Postorder traverse the tree
        * Visit left, then right, then node
        * Recursive
     */
    public void printPostorder(Tree tree) {

        if(tree.left != null) {
            printPostorder(tree.left);
        }

        if(tree.right != null) {
            printPostorder(tree.right);
        }

        System.out.print(tree.data + " "); // print node.
    }

    /*
        * Level order traversal i.e. Breadth First Search!
        * Prints for each level of height of the tree
        * Recursive
     */
    public void printLevelOrder(Tree tree) {

        if(tree == null) {
            return;
        }

        Queue<Tree> nodes = new LinkedList<>();
        nodes.add(tree);

        while(!nodes.isEmpty()) {
            Tree node = nodes.remove();

            System.out.print(node.data + " ");

            if(node.left != null) {
                nodes.add(node.left);
            }

            if(node.right != null) {
                nodes.add(node.right);
            }
        }
    }

}
