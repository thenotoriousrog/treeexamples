/*
    * As one would imagine, this class is used simply for the testing and manipulation of the tree.
 */
public class Driver {

    public static void main(String args[]) {

        Tree root = new Tree(6);
        root.add(4);
        root.add(8);
        root.add(3);
        root.add(5);
        root.add(7);
        root.add(9);

        System.out.println("Printing preorder traversal");
        root.printPreorder(root);
        System.out.println();

        System.out.println("Printing inorder traversal");
        root.printInOrder(root);
        System.out.println();

        System.out.println("Printing postorder traversal");
        root.printPostorder(root);
        System.out.println();

        System.out.println("Printing level order traversal i.e. Breadth First Search");
        root.printLevelOrder(root);
        System.out.println();

    }

}
